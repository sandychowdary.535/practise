<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
<script src="http://code.jquery.com/jquery-1.11.0.min.js"></script>
<script src="http://code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
<script>
	$(document).ready(function() {

		$('#author').change(function() {
			var author = $('#author').val();
			$.ajax({
				type : "Get",
				url : "getBook.action",
				data : "author=" + author,

				success : function(response) {
					var html = '<option value="">--select--</option>';
					$.each(response, function(index, item) {
						html += '<option>'+ item + '</option>';
					});
					$('#books').html(html);
					
				},
				error : function(e) {
					alert('Error: ' + e);
				}
			});
		});

	});
</script>

</head>
<body>
	<center>
		<form:form commandName="book" action="addBook.action">
			<table>
				<tr>
					<td>Author Name</td>
					<td><form:select path="author" id="author">
							<option>--select--</option>
							<form:options items="${authors}"></form:options>
						</form:select></td>
				</tr>
				<tr>
					<td>Book Name</td>
					<td ><form:select path="book" id="books">
							<option>--select--</option>
						</form:select></td>
				</tr>
				<tr>
					<td><input type="submit" value="selectBook" /></td>
					<td><input type="reset" value="reset" /></td>
				</tr>
			</table>
		</form:form>
	</center>
</body>
</html>