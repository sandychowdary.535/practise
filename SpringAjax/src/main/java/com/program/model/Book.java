package com.program.model;

public class Book {
	private String book;
	private String author;

	public Book() {
		super();
	}

	public Book(String book, String author) {
		super();
		this.book = book;
		this.author = author;
	}

	public String getBook() {
		return book;
	}

	public void setBook(String book) {
		this.book = book;
	}

	public String getAuthor() {
		return author;
	}

	public void setAuthor(String author) {
		this.author = author;
	}

	@Override
	public String toString() {
		return "Book [book=" + book + ", author=" + author + "]";
	}

}
