package com.program.model;

public class BookDetails {

	private String bookId;
	private String bookName;
	private String bookDescription;

	public BookDetails() {
		super();
		// TODO Auto-generated constructor stub
	}

	public BookDetails(String bookId, String bookName, String bookDescription) {
		super();
		this.bookId = bookId;
		this.bookName = bookName;
		this.bookDescription = bookDescription;
	}

	public String getBookId() {
		return bookId;
	}

	public void setBookId(String bookId) {
		this.bookId = bookId;
	}

	public String getBookName() {
		return bookName;
	}

	public void setBookName(String bookName) {
		this.bookName = bookName;
	}

	public String getBookDescription() {
		return bookDescription;
	}

	public void setBookDescription(String bookDescription) {
		this.bookDescription = bookDescription;
	}

	@Override
	public String toString() {
		return "BookDetails [bookId=" + bookId + ", bookName=" + bookName
				+ ", bookDescription=" + bookDescription + "]";
	}

}
