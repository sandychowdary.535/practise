package com.program.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.program.model.Book;
import com.program.model.BookDetails;

@Controller
public class BookController {

	@RequestMapping(value = "addBook.action", method = RequestMethod.GET)
	public String addBook(Model model) {
		List<String> authors = new ArrayList<String>();
		authors.add("JRRTolkin");
		authors.add("JKRowling");

		model.addAttribute("book", new Book());
		model.addAttribute("authors", authors);
		return "addBook";
	}

	@RequestMapping(value = "addBook.action", method = RequestMethod.POST)
	public String addBook(@ModelAttribute("book") Book book,
			BindingResult bindingResult, Model model) {
		if (bindingResult.hasErrors()) {
			System.out.println(bindingResult);
		} else

			model.addAttribute("book", book.toString());
		return "forward:index.jsp";
	}

	@RequestMapping(value = "getBook.action", method = RequestMethod.GET)
	public @ResponseBody
	List<String> addBook(@RequestParam("author") String author) {

		List<String> books = new ArrayList<String>();
		if (author.equals("JKRowling")) {
			books.add("HP1");
			books.add("HP2");
			books.add("HP3");
			books.add("HP4");
			books.add("HP5");
			books.add("HP6");
			books.add("HP7");
		} else if (author.equals("JRRTolkin")) {
			books.add("Hobbit");
			books.add("LOR1");
			books.add("LOR2");
			books.add("LOR3");
		}

		return books;
	}
	@RequestMapping(value = "getBookObject.action", method = RequestMethod.GET)
	public @ResponseBody
	List<BookDetails> addBookObject(@RequestParam("author") String author) {

		List<BookDetails> books = new ArrayList<BookDetails>();
		if (author.equals("JKRowling")) {
			books.add(new BookDetails("1", "HP1", "Hogwarts Express"));
			books.add(new BookDetails("2", "HP2", "Tom Riddle"));
			books.add(new BookDetails("3", "HP3", "Padfoot"));
			books.add(new BookDetails("4", "HP4", "Voldamart"));
			books.add(new BookDetails("5", "HP5", "Magic is Might"));
			books.add(new BookDetails("6", "HP6", "Horcruxes"));
			books.add(new BookDetails("7", "HP7", "Hallows"));
		} else if (author.equals("JRRTolkin")) {
			books.add(new BookDetails("8", "Hobbit", "Smag"));
			books.add(new BookDetails("9", "LOR1", "Ring"));
			books.add(new BookDetails("10", "LOR2", "Golum"));
			books.add(new BookDetails("11", "LOR3", "Rawan"));
		}

		return books;
	}

}
