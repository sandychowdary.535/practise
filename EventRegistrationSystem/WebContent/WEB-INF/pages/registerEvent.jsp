<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
<style type="text/css">
.err {
	color: red;
}
</style>
</head>
<body>
	<h1>Register employee for events</h1>
	<hr>
	<center>
		<form:form commandName="registerEvent">
			<table>
				<tr>
					<td>Employees :</td>
					<td><form:select path="employee.mid">
							<form:option value="select">--select--</form:option>
							<form:options items="${employees}" itemLabel="mid" 
							itemValue="mid"></form:options>
						</form:select>
						<form:errors path="employee" cssClass="err" /></td>
				</tr>
				<tr>
					<td>Events:</td>
					<td><form:select path="event.eventId">
							<form:option value="select">--select--</form:option>
							<form:options items="${events}" itemLabel="eventTitle"
								itemValue="eventId"></form:options>
						</form:select>
						<form:errors path="event" cssClass="err" /></td>
				</tr>
				<tr>
					<td><input type="submit" value="Register" /></td>
					<td><input type="button" value="Back"
						onclick="window.location='index.jsp'" /></td>
				</tr>
			</table>
		</form:form>
		<div style="color: green">${message }</div>
		<div style="color: red">${message1 }</div>
	</center>
</body>
</html>