<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
	<h1>List of employees who applied for events</h1>
	<hr>
	<center>
		<table border="1">
			<tr>
				<th>Employee id</th>
				<th>Employee name</th>
				<th>Date of join</th>
				<th>Email id</th>
				<th>Events</th>
			</tr>
			<c:forEach var="s" items="${e}">
				<tr>
					<td>${s.mid}</td>
					<td>${s.name}</td>
					<td><fmt:formatDate type="date" value="${s.joinDate}"/></td>
					<td>${s.email}</td>
					<td><c:choose>
					<c:when test="${s.events.size()!=0}">
					<c:forEach items="${ s.events}" var="eve">
					${eve.eventTitle },
					</c:forEach>
					</c:when>
					<c:otherwise>
					None
					</c:otherwise>
					</c:choose></td>
				</tr>
			</c:forEach>
		</table>
		<a href="index.jsp">Back</a>
	</center>
</body>
</html>