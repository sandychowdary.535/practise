drop database if exists l3_assessment;
create database l3_assessment;
use l3_assessment;

create table employees(
mid varchar(7) primary key,
name varchar(20) not null,
join_date date,
email_id varchar(20));

create table events(
event_id int primary key auto_increment,
event_title varchar(20) not null,
description varchar(60)
);



