package com.program.model;

import java.io.Serializable;

public class RegisterEvent implements Serializable {
/**
	 * 
	 */
private static final long serialVersionUID = 1L;
private Employee employee;
private Event event;
public RegisterEvent() {
	
}
public Employee getEmployee() {
	return employee;
}
public void setEmployee(Employee employee) {
	this.employee = employee;
}
public Event getEvent() {
	return event;
}
public void setEvent(Event event) {
	this.event = event;
}


}
