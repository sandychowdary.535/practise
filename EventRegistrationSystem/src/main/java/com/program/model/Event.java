package com.program.model;

import java.util.Set;

public class Event {
	private long eventId;
	private String eventTitle;
	private String description;
	private Set<Employee> employees;

	public Event() {

	}

	public long getEventId() {
		return eventId;
	}

	public Event(long eventId,String eventTitle, String description) {
		super();
		this.eventId=eventId;
		this.eventTitle = eventTitle;
		this.description = description;
		
	}

	public void setEventId(long eventId) {
		this.eventId = eventId;
	}

	public String getEventTitle() {
		return eventTitle;
	}

	public void setEventTitle(String eventTitle) {
		this.eventTitle = eventTitle;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Set<Employee> getEmployees() {
		return employees;
	}

	public void setEmployees(Set<Employee> employees) {
		this.employees = employees;
	}


	

}
