package com.program.model;

import java.util.Date;
import java.util.Set;

public class Employee {
	private String mid;
	private String name;
	private Date joinDate;
	private String email;
	private Set<Event> events;



	public Employee() {
		
	}

	public Employee(String mid, String name, Date joinDate, String email) {
		super();
		this.mid = mid;
		this.name = name;
		this.joinDate = joinDate;
		this.email = email;

	}

	public String getMid() {
		return mid;
	}

	public void setMid(String mid) {


		this.mid = mid;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Date getJoinDate() {
		return joinDate;
	}

	public void setJoinDate(Date joinDate) {
		this.joinDate = joinDate;
	}

	public Set<Event> getEvents() {
		return events;
	}

	public void setEvents(Set<Event> events) {
		this.events = events;
	}



	

}
