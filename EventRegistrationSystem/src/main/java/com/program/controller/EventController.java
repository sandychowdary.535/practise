package com.program.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.support.DaoSupport;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.mysql.jdbc.exceptions.MySQLIntegrityConstraintViolationException;
import com.program.dao.EventDao;
import com.program.model.RegisterEvent;
import com.program.vaildator.EventValidator;

@Controller
public class EventController {
	@Autowired
	private EventDao eventDao;

	@RequestMapping(value = "/registerEvent.action")
	public ModelAndView registerAEvent() {
		ModelAndView modelAndView = new ModelAndView();
		modelAndView.addObject("employees",eventDao.getEmployees());
		modelAndView.addObject("events", eventDao.getEvents());
		RegisterEvent event=new RegisterEvent();
		modelAndView.addObject("registerEvent",event);
		modelAndView.setViewName("registerEvent");
		return modelAndView;
	}

	@RequestMapping(value = "/registerEvent.action", method = RequestMethod.POST)
	public ModelAndView registerAEvent(
			@ModelAttribute RegisterEvent registerEvent, BindingResult errors) {
		ModelAndView modelAndView = new ModelAndView();
		new EventValidator().validate(registerEvent, errors);
		modelAndView.addObject("employees",eventDao.getEmployees());
		modelAndView.addObject("events", eventDao.getEvents());
		if (errors.hasErrors()) 
		{

		} else {
			modelAndView.addObject("employees",eventDao.getEmployees());
			modelAndView.addObject("events", eventDao.getEvents());
			RegisterEvent event=new RegisterEvent();
			modelAndView.addObject("registerEvent",event);
			try {
				modelAndView.addObject("message", "Registration Sussessfull");
				eventDao.saveRegisteredEvent(registerEvent);
			} catch (Exception e) {
				modelAndView.addObject("message", "You have already registered for this event, please choose a different one");
				//e.printStackTrace();
			}
			
		}
		modelAndView.setViewName("registerEvent");
		return modelAndView;
	}
	@RequestMapping(value="/displayEmployees.action",method=RequestMethod.GET)
	public ModelAndView displayEmployees()
	{
		ModelAndView modelAndView = new ModelAndView();
		modelAndView.addObject("e",eventDao.getEmployees());
		
		modelAndView.setViewName("displayEmployees");
		return modelAndView;

	}
}
