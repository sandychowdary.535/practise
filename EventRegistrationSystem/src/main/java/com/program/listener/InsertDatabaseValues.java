package com.program.listener;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import java.text.ParseException;
import java.text.SimpleDateFormat;

import org.hibernate.Session;
import org.hibernate.Transaction;

import com.program.model.Employee;
import com.program.model.Event;
import com.program.util.HibernateUtil;

public class InsertDatabaseValues implements ServletContextListener {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		Session session = HibernateUtil.getSession();

		SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
		Transaction tx = session.beginTransaction();
		Employee employee1 = null;
		Employee employee2 = null;
		Employee employee3 = null;
		Employee employee4 = null;
		try {

			employee1 = new Employee("M100100", "Karthik Kumar",
					sdf.parse("08-02-2013"), "karthik_kumar");
			employee2 = new Employee("M100108", "Ramesh Kulkarni",
					sdf.parse("05-02-2013"), "ramesh_kulkarni");
			employee3 = new Employee("M100189", "Rohit Agarwal M",
					sdf.parse("22-03-2013"), "rohit_agarwal_m");
			employee4 = new Employee("M101190", "Magesh Narayanan",
					sdf.parse("22-03-2013"), "magesh_narayanan");
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println("Employee details inserted");
		session.saveOrUpdate(employee1);
		session.saveOrUpdate(employee2);
		session.saveOrUpdate(employee3);
		session.saveOrUpdate(employee4);

		Event event1 = new Event(1, "Trekking",
				"Held every month. More details from Manish Kumar.");
		Event event2 = new Event(2, "Guitar Classes",
				"Weekly 3 sessions. Classes conducted by Daniel M");
		Event event3 = new Event(3, "Yoga Classes",
				"Saturdays and Sundays. Classes conducted by Yamini");
		Event event4 = new Event(4, "Health & Diet tips",
				"Every Friday 5 PM to 6 PM by Dr.Kishore Dutta");
		session.saveOrUpdate(event1);
		session.saveOrUpdate(event2);
		session.saveOrUpdate(event3);
		session.saveOrUpdate(event4);
		tx.commit();
		session.close();
		System.out.println("Event details inserted");

	}

	@Override
	public void contextDestroyed(ServletContextEvent arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void contextInitialized(ServletContextEvent arg0) {
		main(null);

	}

}
