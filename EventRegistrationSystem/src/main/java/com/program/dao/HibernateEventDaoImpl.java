package com.program.dao;

import java.util.List;

import javax.management.Query;

import org.springframework.dao.DataAccessException;
import org.springframework.orm.hibernate3.HibernateTemplate;

import com.program.model.Employee;
import com.program.model.Event;
import com.program.model.RegisterEvent;

public class HibernateEventDaoImpl implements EventDao {
	private HibernateTemplate template;

	public void setTemplate(HibernateTemplate template) {
		this.template = template;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Employee> getEmployees() {

		try {
			return template.find("from Employee");
		} catch (DataAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Event> getEvents() {
		// TODO Auto-generated method stub
		try {
			return template.find("from Event");
		} catch (DataAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
	}

	@Override
	public void saveRegisteredEvent(RegisterEvent registerEvent) {
		
			template.save(registerEvent);
	

	}

	@Override
	public List<Employee> getRegisteredEmployees() {
		String hql="select e.mid from Employee e where e.mid join (select mid from RegisterEvent)";
		
		
		try {
			return template.find(hql);
		} catch (DataAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
	}

}
