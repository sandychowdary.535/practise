package com.program.dao;

import java.util.List;

import com.program.model.Employee;
import com.program.model.Event;
import com.program.model.RegisterEvent;

public interface EventDao {
public List<Employee> getEmployees();
public List<Event> getEvents();
public void saveRegisteredEvent(RegisterEvent registerEvent);
public List<Employee> getRegisteredEmployees();
}
