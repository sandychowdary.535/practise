package com.program.POI;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class ReadExcel {

	/**
	 * @param args
	 * @throws IOException
	 */
	public static void main(String[] args) {

		try {
			getDetails();
		} catch (FileNotFoundException e) {

			e.printStackTrace();
		} catch (IOException e) {

			e.printStackTrace();
		}

	}

	private static void getDetails() throws FileNotFoundException, IOException {
		FileInputStream file = new FileInputStream(new File(
				"D:\\ATSBatchProcessorLog.xlsx"));
		XSSFWorkbook xssfWorkbook = new XSSFWorkbook(file);
		XSSFSheet sheet = xssfWorkbook.getSheetAt(0);
		for (Row row : sheet) {
			for (Cell cell : row) {
				System.out.println(cell);
			}
		}
	}

}
