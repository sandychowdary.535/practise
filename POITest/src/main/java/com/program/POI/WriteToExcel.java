package com.program.POI;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;

import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class WriteToExcel {
	private XSSFWorkbook xssfWorkbook;
	private XSSFSheet xssfSheet;
	private XSSFCellStyle style;
	private Row row;
	private Cell cell;
	private XSSFFont hssfFont;

	public WriteToExcel() {
		xssfWorkbook = new XSSFWorkbook();
		xssfSheet = xssfWorkbook.createSheet("ATSBatchProcessor");
		xssfSheet.setColumnWidth(0, 19900);
		xssfSheet.setColumnWidth(1, 19900);

		// creating Main Heading
		xssfSheet.addMergedRegion(new CellRangeAddress(0, 0, 0, 1));
		style = xssfWorkbook.createCellStyle();
		hssfFont = xssfWorkbook.createFont();
		hssfFont.setBold(true);
		hssfFont.setFontHeightInPoints((short) 22);
		hssfFont.setColor(HSSFColor.GREEN.index);
		style.setWrapText(true);
		style.setFillForegroundColor(HSSFColor.GREY_25_PERCENT.index);
		style.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
		style.setVerticalAlignment(XSSFCellStyle.VERTICAL_CENTER);
		style.setAlignment(XSSFCellStyle.ALIGN_CENTER);
		style.setFont(hssfFont);
		row = xssfSheet.createRow(0);
		row.setHeight((short) 500);
		cell = row.createCell(0);
		cell.setCellValue("ATSBatchProcessor");
		cell.setCellStyle(style);

		// creating INFO and DEBUG Heading
		style = xssfWorkbook.createCellStyle();
		hssfFont = xssfWorkbook.createFont();
		hssfFont = xssfWorkbook.createFont();
		hssfFont.setBold(true);
		hssfFont.setFontHeightInPoints((short) 14);
		hssfFont.setColor(HSSFColor.BLUE.index);
		style.setWrapText(true);
		style.setVerticalAlignment(XSSFCellStyle.VERTICAL_CENTER);
		style.setAlignment(XSSFCellStyle.ALIGN_CENTER);
		style.setFont(hssfFont);
		row = xssfSheet.createRow(1);
		cell = row.createCell(0);
		cell.setCellValue("INFO");
		cell.setCellStyle(style);
		cell = row.createCell(1);
		cell.setCellValue("DEBUG");
		cell.setCellStyle(style);
	}

	public static void main(String[] args) {
		String line = null;
		WriteToExcel writeToExcel = new WriteToExcel();
		try {
			writeToExcel.readLogFile(line);
		} catch (IOException e) {

			e.printStackTrace();
		}

	}

	private void readLogFile(String line) throws IOException {
		int info = 2, debug = 2;
		BufferedReader reader = new BufferedReader(new InputStreamReader(
				getClass().getClassLoader().getResourceAsStream(
						"ATSBatchProcessor.log")));

		while ((line = reader.readLine()) != null) {
			if (line.contains("INFO")) {
				enterRecords(line, info, 0);
				info++;
			} else {
				enterRecords(line, debug, 1);
				debug++;
			}
		}
		createExcelSheet();

	}

	private void enterRecords(String data, int rowValue, int cellValue) {
		style = xssfWorkbook.createCellStyle();
		hssfFont = xssfWorkbook.createFont();
		style.setWrapText(true);
		hssfFont.setBold(false);
		style.setFont(hssfFont);
		style.setVerticalAlignment(XSSFCellStyle.VERTICAL_CENTER);
		style.setAlignment(XSSFCellStyle.ALIGN_LEFT);
		if (xssfSheet.getPhysicalNumberOfRows() <= rowValue) {
			row = xssfSheet.createRow(rowValue);
		} else {
			row = xssfSheet.getRow(rowValue);
		}
		cell = row.createCell(cellValue);
		cell.setCellStyle(style);
		cell.setCellValue(data);

	}

	private void createExcelSheet() {
		try {
			FileOutputStream out = new FileOutputStream(new File(
					"D:\\ATSBatchProcessorLog.xlsx"));
			xssfWorkbook.write(out);
			out.close();
			System.out.println("Excel written successfully..");

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
