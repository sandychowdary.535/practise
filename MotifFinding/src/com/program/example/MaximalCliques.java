package com.program.example;

/*
Computer Science 6823 BioInformatics

Spring  2016

Instructor:  Dr Xiuzhen Huang



Description: This program is about motif finding without mutations

Editor/Platform:  vi/Linux



Input:      text containing DNA sequences and length of the motif we are going to found and maximum number of mismatches

Output:     Motif String

Compile:    javac MotifFinding.java

Command:    (After server is running)
        java MotifFinding 

*/
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;

public class MaximalCliques {

	int nodesCount;
	static Map<String, HashSet<String>> graph = new HashMap<String, HashSet<String>>();

	void initGraph(BufferedReader bufReader) throws Exception {
		try {
			nodesCount = Integer.parseInt(bufReader.readLine());
			int edgesCount = Integer.parseInt(bufReader.readLine());
			initGraph();
			for (int k = 0; k < edgesCount; k++) {
				String[] strArr = bufReader.readLine().split(" ");
				String u = strArr[0];
				String v = strArr[1];
				addNbr(u, v);

			}
			for (Map.Entry<String, HashSet<String>> entry : graph.entrySet()) {
				System.out.println("vertex = " + entry.getKey() + ", edges = " + entry.getValue());
			}

		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	void initGraph() {
		graph.clear();
		graph.put("A", new HashSet<String>());
		graph.put("B", new HashSet<String>());
		graph.put("C", new HashSet<String>());
		graph.put("D", new HashSet<String>());
	}

	private void addNbr(String vertU, String vertV) {
		graph.get(vertU).add(vertV);
		graph.get(vertV).add(vertU);
	}

	public static void main(String[] args) {
		BufferedReader br = null;
		try {
			br = new BufferedReader(new FileReader(new File("F:/mc2.txt")));
			MaximalCliques ff = new MaximalCliques();
			BronKerbosch bronKerbosch = new BronKerbosch(graph);

			System.out.println("Max Cliques");

			System.out.println("************** Start Graph ******************************");

			ff.initGraph(br);

			// bronKerbosch.bronKerboschPivotExecute();

		} catch (Exception e) {
			e.printStackTrace();
			System.err.println("Exiting : " + e);
		} finally {
			try {
				br.close();
			} catch (Exception f) {

			}
		}
	}
}
