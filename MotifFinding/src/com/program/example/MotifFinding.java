package com.program.example;

//Java libraries
//First three libraries are to implement message buffer for both in and out messages
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import com.program.example.kmp.KMP;

public class MotifFinding {

	public static void main(String[] args) throws IOException {
		MotifFinding motifFinding = new MotifFinding();
		List<String> dnaSequences = new ArrayList<String>();
		Scanner sc = new Scanner(System.in);

		System.out.println("Enter the length of the motif ..");
		// You have to enter the length of motif string
		int motifLength = sc.nextInt();
		sc.close();
		// Getting DNA sequences from external source
		BufferedReader br = new BufferedReader(new FileReader(new File("E:/dnaSequences.txt")));
		String line = "";

		while ((line = br.readLine()) != null) {
			dnaSequences.add(line);
		}
		br.close();
		// finding motif
		motifFinding.findMotifSequence(motifLength, dnaSequences);
	}

	private void findMotifSequence(int motifLength, List<String> dnaSequences) {
		// getting the initial DNA sequence
		KMP kmp = new KMP();
		String intialSequence = dnaSequences.get(0);
		String motif = "";
		boolean motifFound = true;
		for (int i = 0; i < intialSequence.length(); i++) {
			if ((i + motifLength) > intialSequence.length() || dnaSequences.size() == 1) {
				System.out.println("Motif not found for the motif length : " + motifLength);
				break;
			}
			motifFound = true;
			// tokenizing strings form initial DNA sequence
			motif = intialSequence.substring(i, i + motifLength);
			// searching the each token in next DNA sequences
			for (int j = 1; j < dnaSequences.size(); j++) {
				String sequence = dnaSequences.get(j);
				if (!kmp.KMPSearch(motif, sequence))
					motifFound = false;

			}
			// if that particular token found in every DNA sequence then we
			// found our Motif
			if (motifFound) {
				System.out.println("Motif found for the motif length : " + motifLength + " and the motif is " + motif);
				break;
			}
		}

	}
}
