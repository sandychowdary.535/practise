package com.program.example;

/*


Computer Science 6823 BioInformatics

Spring  2016

Instructor:  Dr Xiuzhen Huang



Description: This program is about motif finding without mutations

Editor/Platform:  vi/Linux



Input:      text containing DNA sequences and length of the motif we are going to found and maximum number of mismatches

Output:     Motif String

Compile:    javac MotifFinding.java

Command:    (After server is running)
        java MotifFinding 

*/
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class MotifFindingWithMutation {
	public static void main(String[] args) throws IOException {
		MotifFindingWithMutation motifFinding = new MotifFindingWithMutation();
		List<String> dnaSequences = new ArrayList<String>();
		Scanner sc = new Scanner(System.in);

		System.out.println("Enter the length of the motif ..");
		int motifLength = sc.nextInt();
		System.out.println("Enter the maximum allowed missmatches ..");
		int allowedMissmatches = sc.nextInt();

		BufferedReader br = new BufferedReader(new FileReader(new File("F:/dnaSequencesWithMutations.txt")));

		String line = "";
		while ((line = br.readLine()) != null) {
			dnaSequences.add(line);
		}
		sc.close();
		br.close();

		motifFinding.findMotifSequence(motifLength, dnaSequences, allowedMissmatches);
	}

	private void findMotifSequence(int motifLength, List<String> dnaSequences, int allowedMissmatches) {
		List<String> matches = new ArrayList<String>();
		String intialSequence = dnaSequences.get(0);
		String motif = "";
		boolean motifFound = true;
		for (int i = 0; i < intialSequence.length(); i++) {
			if ((i + motifLength) > intialSequence.length() || dnaSequences.size() == 1) {
				System.out.println("Motif not found for the motif length : " + motifLength);
				break;
			}
			motifFound = true;
			motif = intialSequence.substring(i, i + motifLength);
			matches.clear();
			matches.add(motif);
			for (int j = 1; j < dnaSequences.size(); j++) {
				if (!comapreMotifs(motifLength, dnaSequences.get(j), motif, allowedMissmatches, matches))
					motifFound = false;
			}

			if (motifFound) {
				break;
			}

		}
		for (String moti : matches) {
			System.out.println(moti);
		}
		findMotif(matches, motifLength);
	}

	public boolean comapreMotifs(int motifLength, String sequence, String motif, int allowedMissmatches,
			List<String> matches) {
		boolean found = false;
		for (int i = 0; i < sequence.length(); i++) {
			if ((i + motifLength) > sequence.length())
				continue;
			String possibleMotif = sequence.substring(i, i + motifLength);
			int count = 0;
			for (int j = 0; j < motifLength; j++) {

				if (!(motif.charAt(j) == possibleMotif.charAt(j)))
					count++;
			}

			if (count <= allowedMissmatches) {
				matches.add(possibleMotif);
				found = true;
			}

		}
		return found;
	}

	private void findMotif(List<String> matches, int motifLength) {
		System.out.println("________________");
		String motifString = "";
		int a = 0, g = 0, t = 0, c = 0;
		// System.out.println("a" + " " + "g" + " " + "t" + " " + "c" + " " +
		// "max");
		for (int i = 0; i < motifLength; i++) {
			for (String motif : matches) {
				char base = motif.toLowerCase().charAt(i);
				if (base == 'a')
					a++;
				if (base == 'g')
					g++;
				if (base == 't')
					t++;
				if (base == 'c')
					c++;

			}
			int max = max(a, g, t, c);
			if (max == a)
				motifString += 'a';
			else if (max == g)
				motifString += 'g';
			else if (max == t)
				motifString += 't';
			else if (max == c)
				motifString += 'c';
			// System.out.println(a + " " + g + " " + t + " " + c + " "+max );
			a = 0;
			g = 0;
			t = 0;
			c = 0;

		}
		System.out.println(motifString);
		System.out.println("________________");
	}

	public static int max(int a, int b, int c, int d) {
		return (a > b && a > c && a > d ? a : b > c && b > d ? b : c > d ? c : d);
	}

}
