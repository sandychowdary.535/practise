package com.program.example;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

public class BronKerbosch {
	Map<String, HashSet<String>> graph;

	public BronKerbosch(Map<String, HashSet<String>> graph) {
		this.graph = graph;
	}

	public void bronKerboschPivotExecute() {

		HashSet<String> X = new HashSet<String>();
		HashSet<String> R = new HashSet<String>();

		List<String> vertices = new ArrayList<String>();
		for (Map.Entry<String, HashSet<String>> entry : graph.entrySet()) {
			vertices.add(entry.getKey());
		}
		HashSet<String> P = new HashSet<String>(vertices);
		bronKerboschWithoutPivot(R, P, X, "");
	}

	private HashSet<String> getNeighbors(String v) {
		return graph.get(v);
	}

	private HashSet<String> intersect(HashSet<String> arlFirst, HashSet<String> arlSecond) {
		HashSet<String> arlHold = new HashSet<String>(arlFirst);
		arlHold.retainAll(arlSecond);
		return arlHold;
	}

	private void bronKerboschWithoutPivot(HashSet<String> R, HashSet<String> P, HashSet<String> X, String pre) {

		System.out.print(pre + " " + printSet(R) + ", " + printSet(P) + ", " + printSet(X));
		if ((P.size() == 0) && (X.size() == 0)) {
			printClique(R);
			return;
		}
		System.out.println();

		HashSet<String> P1 = new HashSet<String>(P);

		for (String v : P) {
			R.add(v);
			bronKerboschWithoutPivot(R, intersect(P1, getNeighbors(v)), intersect(X, getNeighbors(v)), pre + "\t");
			R.remove(v);
			P1.remove(v);
			X.add(v);
		}
	}

	private void printClique(HashSet<String> R) {
		System.out.print("  -------------- Maximal Clique : ");
		for (String v : R) {
			System.out.print(" " + (v));
		}
		System.out.println();
	}

	private String printSet(HashSet<String> Y) {
		StringBuilder strBuild = new StringBuilder();

		strBuild.append("{");
		for (String v : Y) {
			strBuild.append("" + (v) + ",");
		}
		if (strBuild.length() != 1) {
			strBuild.setLength(strBuild.length() - 1);
		}
		strBuild.append("}");
		return strBuild.toString();
	}
}
