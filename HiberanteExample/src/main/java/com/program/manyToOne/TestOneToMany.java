package com.program.manyToOne;

import java.util.HashSet;
import java.util.Set;

import org.hibernate.Session;
import org.hibernate.Transaction;

import com.program.util.HibernateUtil;

/*
 * Drop the tables in DB before running this class
 * 
 */
public class TestOneToMany {

	public static void main(String[] args) {
		/*
		 * one department to many students
		 */
		Student student1 = new Student(535, "sandy", new Department(1, "CSE"));
		Student student2 = new Student(518, "Kiran", new Department(1, "CSE"));
		Student student3 = new Student(557, "Varsha", new Department(4, "AERO"));

		Set<Student> students1 = new HashSet<Student>();
		students1.add(student1);
		students1.add(student2);
		Set<Student> students2 = new HashSet<Student>();
		students2.add(student3);
		Department department1 = new Department(1, "CSE", students1);
		Department department2 = new Department(4, "AERO", students2);
		Session session2 = HibernateUtil.getNewSession();
		Transaction transaction = session2.beginTransaction();
		session2.saveOrUpdate(department1);
		session2.saveOrUpdate(department2);
		transaction.commit();
		session2.close();
	}

}
