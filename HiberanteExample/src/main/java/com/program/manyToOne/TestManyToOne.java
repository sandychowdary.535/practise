package com.program.manyToOne;

import org.hibernate.Session;
import org.hibernate.Transaction;

import com.program.util.HibernateUtil;

/*
 * Drop the tables in DB before running this class
 * 
 */
public class TestManyToOne {

	public static void main(String[] args) {

		/**
		 * Many Students to one department
		 */
		Department department1 = new Department(1, "CSE");
		Department department2 = new Department(2, "ECE");
		Department department3 = new Department(3, "MECH");
		Department department4 = new Department(4, "AERO");
		Session session = HibernateUtil.getNewSession();
		Transaction transaction = session.beginTransaction();
		session.saveOrUpdate(department1);
		session.saveOrUpdate(department2);
		session.saveOrUpdate(department3);
		session.saveOrUpdate(department4);
		transaction.commit();
		session.close();

		Student student1 = new Student(535, "sandy", department1);// Many to one
																	// to
																	// department1
		Student student3 = new Student(505, "Baru", new Department(0,"CSE"));// Many to one
																	// to
																	// department1
		Student student2 = new Student(518, "Kiran", department2);
		Session session1 = HibernateUtil.getNewSession();
		Transaction transaction1 = session1.beginTransaction();
		session1.saveOrUpdate(student1);
		session1.saveOrUpdate(student2);
		session1.saveOrUpdate(student3);
		transaction1.commit();
		session1.close();

	}
}
