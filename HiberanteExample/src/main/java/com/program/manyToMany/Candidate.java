package com.program.manyToMany;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

@Table
@Entity
public class Candidate {
	@Id
	@Column(name = "candidate_id")
	private int candidateId;
	@Column(name = "candidate_name")
	private String candidateName;
	@ManyToMany(cascade = { CascadeType.ALL })
	@JoinTable(name = "candidate_course", joinColumns = { @JoinColumn(name = "candidate_id") }, inverseJoinColumns = { @JoinColumn(name = "course_id") })
	Set<Course> courses = new HashSet<Course>();

	public Candidate() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Candidate(int candidateId, String candidateName, Set<Course> courses) {
		super();
		this.candidateId = candidateId;
		this.candidateName = candidateName;
		this.courses = courses;
	}

	public int getCandidateId() {
		return candidateId;
	}

	public void setCandidateId(int candidateId) {
		this.candidateId = candidateId;
	}

	public String getCandidateName() {
		return candidateName;
	}

	public void setCandidateName(String candidateName) {
		this.candidateName = candidateName;
	}

	public Set<Course> getCourses() {
		return courses;
	}

	public void setCourses(Set<Course> courses) {
		this.courses = courses;
	}

	@Override
	public String toString() {
		return "Candidate [candidateId=" + candidateId + ", candidateName="
				+ candidateName + ", courses=" + courses + "]";
	}

}
