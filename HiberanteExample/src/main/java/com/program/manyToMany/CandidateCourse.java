package com.program.manyToMany;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "candidate_course")
public class CandidateCourse implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	@Column(name = "candidate_id")
	private int candidateId;
	@Id
	@Column(name = "course_id")
	private int courseId;

	@Override
	public String toString() {
		return "CandidateCourse [candidateId=" + candidateId + ", courseId="
				+ courseId + "]";
	}

	public int getCandidateId() {
		return candidateId;
	}

	public void setCandidateId(int candidateId) {
		this.candidateId = candidateId;
	}

	public int getCourseId() {
		return courseId;
	}

	public void setCourseId(int courseId) {
		this.courseId = courseId;
	}

	public CandidateCourse() {
		super();
		// TODO Auto-generated constructor stub
	}

	public CandidateCourse(int candidateId, int courseId) {
		super();
		this.candidateId = candidateId;
		this.courseId = courseId;
	}

}
