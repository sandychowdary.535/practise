package com.program.manyToMany;

import java.util.HashSet;
import java.util.Set;

import org.hibernate.Session;
import org.hibernate.Transaction;
import com.program.util.HibernateUtil;

public class TestManyToMany {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		Course course1 = new Course(1, "JAVA");
		Course course2 = new Course(2, "PHP");
		Set<Course> courses = new HashSet<Course>();
		courses.add(course1);
		courses.add(course2);
		Session session = HibernateUtil.getNewSession();
		Transaction transaction = session.beginTransaction();
		session.save(new Candidate(1, "sandy", courses));
		transaction.commit();
		session.close();

	}

}
