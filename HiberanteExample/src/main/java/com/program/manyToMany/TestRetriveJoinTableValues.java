package com.program.manyToMany;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

import com.program.util.HibernateUtil;

public class TestRetriveJoinTableValues {
	public static void main(String[] args) {
		Session session = HibernateUtil.getNewSession();
		Transaction transaction = session.beginTransaction();
		Query query = session.createQuery("from CandidateCourse");
		@SuppressWarnings("unchecked")
		List<CandidateCourse> candidateCourses = query.list();
		System.out.println(candidateCourses.size());
		for (CandidateCourse candidateCourse : candidateCourses) {
			System.out.println(candidateCourse.toString());
		}
		transaction.commit();
		session.close();
	}
}
