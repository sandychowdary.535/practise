package com.program.util;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.AnnotationConfiguration;

public class HibernateUtil {
	public static Session getNewSession(){
		// creates an empty configuration object
		AnnotationConfiguration cfg = new AnnotationConfiguration();
		
		// read the hibernate.cfg.xml
		cfg.configure();
		
		// get the information about session-factory
		SessionFactory factory = cfg.buildSessionFactory();
		
		return factory.openSession();
	}
}
